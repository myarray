#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "myarray.h"

MArray myarray_new(int size, int incr, int elsize) {
    MArray myarray = malloc(sizeof(struct myarray));
    myarray->elements = malloc(size * elsize);
    myarray->len = 0;
    myarray->maxlen = size;
    myarray->incr = incr;
    myarray->elsize = elsize;
    myarray->cmpfunc = NULL;

    return myarray;
}

void myarray_set_cmpfunc(MArray array, int (*cmpfunc)(void *, void *)) {
    array->cmpfunc = cmpfunc;
}

void myarray_enlarge(MArray array) {
    array->maxlen+= array->incr;
    array->elements = realloc(array->elements, array->maxlen*array->elsize);
}

void _myarray_insert(MArray array, void *element, int index) {
    if (index < 0 || index > array->len) {
        fprintf(stderr, "Array index out of bounds %s:%d. Index: %d. Array size: %d\n", __FILE__, __LINE__, index, array->len);
        exit(EXIT_FAILURE);
    }
    if (array->len == array->maxlen) {
        myarray_enlarge(array);
    }
    memmove(array->elements + (index + 1) * array->elsize, array->elements + index * array->elsize, (array->len - index)*array->elsize);
    void *toinsert = array->elements + (index * array->elsize);
    memcpy(toinsert, element, array->elsize);
    array->len += 1;
}

void myarray_insert_sorted_helper(MArray array, void *element, int left, int right) {
    int index;
    int retVal;

    if (left > right) {
        _myarray_insert(array, element, left);
        return;
    }

    index = (left + right) / 2;
    retVal = array->cmpfunc(element, array->elements + index * array->elsize);

    if (retVal == 0) {
        _myarray_insert(array, element, index);
    } else if (retVal < 0) {
        myarray_insert_sorted_helper(array, element, left, index - 1);
    } else {
        myarray_insert_sorted_helper(array, element, index + 1, right);
    }
}

void _myarray_insert_sorted(MArray array, void *element) {
    if (array->cmpfunc == NULL) {
        _myarray_insert(array, element, array->len);
    } else {
        myarray_insert_sorted_helper(array, element, 0, array->len - 1);
    }
}

void myarray_remove_index(MArray array, int index) {
    memmove(array->elements + index * array->elsize, array->elements + (index + 1) * array->elsize, (array->len - index - 1)*array->elsize);
    array->len--;
}

void myarray_free(MArray array) {
    free(array->elements);
    free(array);
}

void myarray_reset(MArray array) {
    array->len = 0;
}
